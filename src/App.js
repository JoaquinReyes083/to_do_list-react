import React, { useState } from "react";
import Form from "./Form";
import "./App.css";
import logo from './logo.svg';

export default () => {
  const [todos, setTodos] = useState([]);

  const toggleComplete = i =>
    setTodos(
      todos.map(
        (todo, k) =>
          k === i
            ? {
                ...todo,
                complete: !todo.complete
              }
            : todo
      )
    );

  return (
    <div className="App">
       <img src={logo} className="App-logo" alt="logo" />
       <h2 className="h2">To Do List in React :)</h2>
       <p className="red">The color red means not completed.</p>
       <p className="green">The color green means completed.</p>
      <Form
        onSubmit={text => setTodos([{ text, complete: false }, ...todos])}
      />
      <div>
        {todos.map(({ text, complete }, i) => (
          <div
            key={text}
            onClick={() => toggleComplete(i)}
            style={{
              color: complete ? "#00e600" : ""
            }}
          >
            {text}
          </div>
        ))}
      </div>
      <button onClick={() => setTodos([])}>Delete All</button>
    </div>
  );
};